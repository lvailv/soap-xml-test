package com.didadiandi.soapXml.domain.parse;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import lombok.Data;


@Data
@XmlAccessorType(value = XmlAccessType.FIELD)
public class TransReplyClass {

    private Boolean transactionCompleted;

    private String retData;
}
