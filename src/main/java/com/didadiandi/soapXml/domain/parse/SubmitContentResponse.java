package com.didadiandi.soapXml.domain.parse;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;


@Getter
@XmlRootElement(name = "SubmitContentResponse", namespace = "http://www.w3.org/")
public class SubmitContentResponse {

    private TransReplyClass transReplyClass;


    @XmlElement(name = "TransReplyClass")
    public void setTransReplyClass(TransReplyClass transReplyClass) {
        this.transReplyClass = transReplyClass;
    }
}
