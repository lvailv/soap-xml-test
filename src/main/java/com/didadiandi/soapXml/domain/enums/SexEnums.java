package com.didadiandi.soapXml.domain.enums;


import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;

@XmlEnum(value = String.class)
public enum SexEnums {

    @XmlEnumValue(value = "MAN")
    MAN,
    @XmlEnumValue(value = "WOMAN")
    WOMAN
}
