package com.didadiandi.soapXml.domain.constant;

/**
 * Description:
 * <p>
 * ClassName: ConstantXML
 * date : 2022/2/22 10:20
 *
 * @author lazye
 * @version 1.0
 * @since JDK 1.8
 */
public interface ConstantXML {

    String resultResponse = """
        <?xml version='1.0' encoding='UTF-8'?>
        <S:Envelope
            xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
            <SOAP-ENV:Header/>
            <S:Body xmlns:ns2="http://www.w3.org/">
                <ns2:SubmitContentResponse xmlns:ns2="http://www.w3.org/">
                    <TransReplyClass>
                        <transactionCompleted>true</transactionCompleted>
                        <retData>1436248</retData>
                    </TransReplyClass>
                </ns2:SubmitContentResponse>
            </S:Body>
        </S:Envelope>
        """;
}
